<?php

return [

    'default' => env('SMS_DRIVER', 'ghasedak'),

    'drivers' => [

        'ghasedak' => [
            'apiKey' => '3e666f24db07b002a93f712cc69b41c22bd398248b40a19e3059d0b767970211',
            'from' => '300002525',
        ],

        'kavenegar' => [
            'apiKey' => '665577397439556D302F4F6B334C442B643545514B342B55506F7758307856446D6F4E74594C3276545A733D',
            'from' => '0018018949161',
        ]

    ],


    'text' => [
        'increase' => 'مبلغ %amount% به حساب شما واریز گردید',
        'decrease' => 'مبلغ %amount% از حساب شما کسر گردید'
    ],

    'map' => [
        'kavenegar' => \App\Drivers\Kavenegar::class,
        'ghasedak' => \App\Drivers\Ghasedak::class,
    ],
];
