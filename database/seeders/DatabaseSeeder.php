<?php

namespace Database\Seeders;

use App\Models\BankAccount;
use App\Models\BankCard;
use App\Models\BankFee;
use App\Models\Transaction;
use App\Models\Transfer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         \App\Models\User::factory(20)
             ->has(BankAccount::factory()->has(BankCard::factory()->count(2))->count(2))
             ->create();

         Transfer::factory(20)->has(Transaction::factory()->count(2))->has(BankFee::factory()->count(1))->create();

    }
}
