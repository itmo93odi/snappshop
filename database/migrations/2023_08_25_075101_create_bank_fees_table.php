<?php

use App\Models\Transfer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bank_fees', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('transfer_id');

            $table->decimal('fee', 64, 0)->default(0);

            $table->foreign('transfer_id')
                ->references('id')
                ->on($this->trasferTable())
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bank_fees');
    }

    private function trasferTable(): string
    {
        return (new Transfer())->getTable();
    }
};
