<?php

namespace Database\Factories;

use App\Models\BankCard;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transfer>
 */
class TransferFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'from_id' => BankCard::all()->random(),
            'from_type' => function (array $attributes) {
                return BankCard::find($attributes['from_id'])->getMorphClass();
            },
            'to_id' => BankCard::all()->random(),
            'to_type' => function (array $attributes) {
                return BankCard::find($attributes['to_id'])->getMorphClass();
            },
            'status' => 'transfer',
            'fee'    => fake()->randomFloat(2,10000,999999)
        ];
    }
}
