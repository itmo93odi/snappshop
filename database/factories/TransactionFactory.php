<?php

namespace Database\Factories;

use App\Models\BankCard;
use App\Models\Transfer;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'payable_id' => BankCard::all()->random(),
            'payable_type' => function (array $attributes) {
                return BankCard::find($attributes['payable_id'])->getMorphClass();
            },
            'type' => fake()->randomElement(['withdraw','deposit']),
            'transfer_id' => Transfer::all()->random(),
            'amount' => fake()->randomFloat(2,10000,999999)
        ];
    }
}
