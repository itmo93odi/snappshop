<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\BankCard>
 */
class BankCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => fake()->creditCardNumber(),
            'cvv2' => fake()->randomNumber(4) ,
            'amount' => fake()->randomFloat(2,1000,9999999),
            'expire_at' => fake()->date('Y-m-d')
        ];
    }
}
