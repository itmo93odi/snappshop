<?php


namespace App\Traits;


use App\Models\BankCard;
use App\Service\PrepareService;
use App\Service\TransferService;

/**
 * Trait WalletTrait
 * @package App\Traits
 */
trait HasCard
{

    public function deposit(float|int $amount)
    {
        return app(PrepareService::class)->deposit($this , $amount);
    }

    public function withdraw(float $amount)
    {
        return app(PrepareService::class)->withdraw();
    }

    public function transfer(BankCard $destinationCard,float $amount){

        return app(TransferService::class)->apply($this,$destinationCard,$amount);
    }


}
