<?php

namespace App\Http\Requests;

use App\Rules\CheckCardBank;
use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'origincard' => ['required' , 'exists:bank_cards,number',new CheckCardBank()],
            'destinationcard' => ['required' , 'exists:bank_cards,number', new CheckCardBank()],
            'amount' => 'required|integer|between:1000,50000000'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'origincard' => convert_number($this->origincard),
            'destinationcard' => convert_number($this->destinationcard),
        ]);
    }
}
