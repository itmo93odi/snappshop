<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Repository\UserRepository;

class TransactionController extends Controller
{

    public function __construct(
        private readonly UserRepository $userRepository
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

         $users=$this->userRepository->getUserWithTransaction();

         return UserResource::collection($users);
    }

}
