<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferRequest;
use App\Repository\BankCardRepository;

class TransferController extends Controller
{

    public function __construct(
        private readonly BankCardRepository $bankCardRepository)
    {
    }

    /**
     * Show the form for creating a new resource.
     */
    public function transfer(TransferRequest $request)
    {
        $cardOrigin=request('origincard');
        $cardDestination=request('destinationcard');
        $amount=request('amount');

        $origin=$this->bankCardRepository->getCardWithNumber($cardOrigin);
        $destination=$this->bankCardRepository->getCardWithNumber($cardDestination);

        $origin->transfer($destination,$amount);

        return response()->json([
            'data' => [
                'message' => __('message.transfer')
            ]
        ]);
    }
}
