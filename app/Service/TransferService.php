<?php

namespace App\Service;

use App\Facades\Sms;
use App\Models\BankCard;
use App\Models\BankFee;
use App\Repository\BankFeeRepository;
use App\Repository\TransactionRepository;
use App\Repository\TransferRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TransferService
{

    public function __construct(
        private readonly PrepareService $prepareService ,
        private readonly TransactionRepository $transactionRepository ,
        private readonly TransferRepository $transferRepository,
        private readonly BankFeeRepository $bankFeeRepository,
        private readonly UserRepository $userRepository,
    )
    {
    }

    public function apply(BankCard $originCard , BankCard $destinationCard , $amount){


        DB::transaction(function () use ($originCard,$destinationCard, $amount) {

            $amountFee=$amount+BankFee::FEE;

            $this->prepareService->withdraw($originCard,$amountFee);

            $transfer=$this->transferRepository->create([
                'from_type' => $originCard::class ,
                'from_id'   =>  $originCard->id,
                'to_type' =>  $destinationCard::class ,
                'to_id'   =>  $destinationCard->id,
                'status'   => 'transfer' ,
                'fee'   => $amount ,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);

            $this->bankFeeRepository->create([
                'transfer_id' => $transfer->id,
                'fee'         => BankFee::FEE
            ]);

            $this->transactionRepository->insert([
                [
                    'payable_type' => $originCard::class ,
                    'payable_id'   => $originCard->id ,
                    'type'         => 'withdraw' ,
                    'amount'       => $amountFee ,
                    'transfer_id'  => $transfer->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
                [
                    'payable_type' => $destinationCard::class ,
                    'payable_id'   => $destinationCard->id ,
                    'type'         => 'deposit' ,
                    'amount'       => $amount ,
                    'transfer_id'  => $transfer->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ],
            ]);

            $text=config('sms.text.increase');
            $text=str_replace('%amount%',$amount,$text);
            $user=$this->userRepository->getUserWithCard($destinationCard->number);

            Sms::send($text)->to([$user->mobile])->dispatch();
        });


    }

}
