<?php

namespace App\Service;

use App\Exceptions\AmountInvalid;
use App\Models\BankCard;
use App\Repository\BankCardRepository;
use Brick\Math\BigDecimal;

class PrepareService
{
    public function __construct(
        private readonly BankCardRepository $repositoryCartBank
    )
    {
    }

    public function deposit(BankCard $card , float|int $amount){
        $this->checkAmount($card->amount,$amount);
        $this->repositoryCartBank->updateAmount($card,$amount);
    }

    public function withdraw(BankCard $card , float|int $amount){
        $this->checkAmount($card->amount,$amount);
        $this->repositoryCartBank->updateAmount($card,-$amount);
    }



    public function checkAmount($firstAmount,$secondAmount){
        $result=BigDecimal::of($firstAmount)->compareTo(BigDecimal::of($secondAmount));
        if ($result === -1) {
            throw new AmountInvalid('Amount Invalid');
        }
    }


}
