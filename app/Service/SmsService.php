<?php

namespace App\Service;

class SmsService
{
    protected array $config;

    protected array $settings;

    protected string $driver;

    protected BuilderService $builder;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->setBuilder(new BuilderService());
        $this->via($this->config['default']);
    }

    public function to($recipients): self
    {
        $this->builder->to($recipients);

        return $this;
    }

    public function via($driver): self
    {
        $this->driver = $driver;
        $this->builder->via($driver);
        $this->settings = $this->config['drivers'][$driver];

        return $this;
    }

    public function send($message)
    {

        $this->builder->send($message);

        return $this;
    }

    public function dispatch()
    {
        $this->driver = $this->builder->getDriver() ?: $this->driver;
        if (empty($this->driver)) {
            $this->via($this->config['default']);
        }
        $driver = $this->getDriverInstance();
        $driver->message($this->builder->getBody());
        $driver->to($this->builder->getRecipients());

        return $driver->send();
    }

    protected function setBuilder(BuilderService $builder): self
    {
        $this->builder = $builder;

        return $this;
    }

    protected function getDriverInstance()
    {
        $class = $this->config['map'][$this->driver];

        return new $class($this->settings);
    }

}
