<?php

namespace App\Drivers;

use Kavenegar\KavenegarApi;

class Kavenegar extends Driver
{
    protected KavenegarApi $client;

    public function send()
    {
        try {
            $response = collect();
            $this->client = new KavenegarApi(data_get($this->settings, 'apiKey'));
            foreach ($this->recipients as $recipient) {

                $result=$this->client->Send($this->sender, $recipient, $this->body);

                $response->put($recipient, $result);
            }

            return (count($this->recipients) == 1) ? $response->first() : $response;
        } catch (\Exception $exception){
            abort($exception->getCode(),$exception->getMessage());
        }

    }
}
