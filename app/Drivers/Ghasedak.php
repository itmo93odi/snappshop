<?php

namespace App\Drivers;

use Ghasedak\GhasedakApi;
use Illuminate\Support\Facades\Log;

class Ghasedak extends Driver
{
    protected GhasedakApi $client;



    public function send()
    {
        try {
            $response = collect();
            $this->client = new GhasedakApi( data_get($this->settings, 'apiKey'));

            foreach ($this->recipients as $recipient) {
                $result=$this->client->SendSimple($recipient,$this->body);

                $response->put($recipient, $result);
            }

            return (count($this->recipients) == 1) ? $response->first() : $response;
        }catch (\Exception $exception){
            Log::error($exception->getMessage());
        }

    }
}
