<?php

namespace App\Repository;


use App\Models\BankFee;

class BankFeeRepository
{

    public function __construct(
        private readonly BankFee $bankFee
    )
    {
    }

    public function create(array $object)
    {
        return $this->bankFee->newQuery()->create($object);
    }
}
