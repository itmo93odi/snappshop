<?php

namespace App\Repository;


use App\Models\Transaction;

class TransactionRepository
{

    public function __construct(
        private readonly Transaction $transaction
    )
    {
    }

    public function insert(array $object): bool
    {
        return $this->transaction->newQuery()->insert($object);
    }
}
