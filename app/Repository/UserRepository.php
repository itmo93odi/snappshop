<?php

namespace App\Repository;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository
{

    public function getUserWithCard(string $cardNumber)
    {
        return User::withWhereHas('bankAccount.bankCard', function ($query) use ($cardNumber) {
            $query->where('number', $cardNumber);
        })->first();
    }

    public function getUserWithTransaction()
    {
        return User::query()
            ->with('bankCard')
            ->select('users.*', DB::raw('count(*) as count'))
            ->rightJoin('bank_accounts as ba', 'users.id', '=', 'ba.user_id')
            ->rightJoin('bank_cards as bc', 'ba.id', '=', 'bc.account_id')
            ->rightJoin('transactions as trans', 'bc.id', '=', 'trans.payable_id')
            ->groupBy('users.id')
            ->orderBy('count', 'DESC')
            ->limit(3)
            ->get();
    }
}
