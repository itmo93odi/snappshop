<?php

namespace App\Repository;


use App\Models\Transfer;

class TransferRepository
{

    public function __construct(
        private readonly Transfer $transfer
    )
    {
    }

    public function create(array $object)
    {
        return $this->transfer->newQuery()->create($object);
    }
}
