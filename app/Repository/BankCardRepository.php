<?php

namespace App\Repository;

use App\Models\BankCard;
use Illuminate\Support\Facades\DB;

class BankCardRepository
{


    public function __construct(
        private readonly BankCard $bankCard
    )
    {
    }

    public function updateAmount(BankCard $card,$amount): bool
    {
        return $card->update([
            'amount' => DB::raw('amount+'.$amount)
        ]);
    }

    public function getCardWithNumber($number){
        return $this->bankCard->whereNumber($number)->first();
    }
}
