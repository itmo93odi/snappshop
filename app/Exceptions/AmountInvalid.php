<?php

namespace App\Exceptions;

use InvalidArgumentException;

final class AmountInvalid extends InvalidArgumentException
{
}
