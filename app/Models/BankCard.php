<?php

namespace App\Models;

use App\Traits\HasCard;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class BankCard extends Model
{
    use HasFactory,HasCard;

    protected $fillable = [
        'number',
        'cvv2',
        'amount',
        'expire_at',
        'account_id'
    ];

    /**
     * Get the post's image.
     */
    public function transaction(): MorphOne
    {
        return $this->morphOne(Transaction::class, 'payable');
    }

    public function bankAccount(){
        return $this->belongsTo(BankAccount::class,'account_id');
    }
}
