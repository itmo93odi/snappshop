<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'payable_type',
        'payable_id',
        'type',
        'amount',
        'transfer_id'
    ];

    /**
     * Get the parent imageable model (user or post).
     */
    public function payable(): MorphTo
    {
        return $this->morphTo();
    }
}
