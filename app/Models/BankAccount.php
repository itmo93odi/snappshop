<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    use HasFactory;

    public function bankCard(){
        return $this->hasMany(BankCard::class,'account_id');
    }

    public function user(){
        return $this->belongsTo(User::class , 'user_id');
    }
}
