<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $fillable = [
        'from_type',
        'from_id',
        'to_type',
        'to_id',
        'status',
        'fee',
    ];

    public function transaction(){
        return $this->hasMany(Transaction::class , 'transfer_id');
    }

    public function bankFee(){
        return $this->hasOne(BankFee::class , 'transfer_id');
    }
}
