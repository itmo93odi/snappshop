<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankFee extends Model
{
    use HasFactory;
    const FEE = 500;

    protected $fillable = [
        'transfer_id',
        'fee'
    ];
}
