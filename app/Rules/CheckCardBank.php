<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckCardBank implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $card = (string) preg_replace('/\D/','',$value);
        $strlen = strlen($card);
        for($i=0; $i<$strlen; $i++)
        {
            $res[$i] = $card[$i];
            if(($strlen%2)==($i%2))
            {
                $res[$i] *= 2;
                if($res[$i]>9)
                    $res[$i] -= 9;
            }
        }
        if(!in_array($card[0],[2,4,5,6,9]) || $strlen!=16 || array_sum($res)%10 != 0 ) {
            $fail('validation.card')->translate();
        }
    }
}
