<?php

namespace App\Providers;

use App\Service\SmsService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind('service-sms', function () {
            return new SmsService(config('sms'));
        });
    }
}
